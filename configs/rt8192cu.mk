# ---------------------------------------------------------------
# Build rt8192cu
# ---------------------------------------------------------------

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

$(RT8192CU_T)-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(KSRC)" = "" ] || [ ! -d $(KSRC) ]; then \
		$(MSG11) "Can't find kernel tree.  Try setting KSRC= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ ! -d $(KSRC)/modules/lib/modules/ ]; then \
		$(MSG11) "Can't find kernel modules tree.  Try setting KSRC= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(RT8192CU_T)-get $(RT8192CU_T)-get: 
	@mkdir -p $(BLDDIR)
	@if [ ! -d $(RT8192CU_SRCDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving RT8192CU source" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(BLDDIR) && git clone $(RT8192CU_URL) $(RT8192CU_VERSION); \
	else \
		$(MSG3) "Source is cached in $(RT819CU_SRCDIR)/$(RT8192CU_VERSION) - doing pull" $(EMSG); \
		cd $(RT8192CU_SRCDIR)/$(RT8192CU_VERSION) && git pull; \
	fi; \
	$(MSG3) "Checking out branch: $(RT8192CU_GIT_ID)" $(EMSG)
	cd $(RT8192CU_SRCDIR) && git checkout $(RT8192CU_GIT_ID)
	@touch .$(subst .,,$@)

.$(RT8192CU_T)-get-patch $(RT8192CU_T)-get-patch: .$(RT8192CU_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(RT8192CU_T)-unpack $(RT8192CU_T)-unpack: .$(RT8192CU_T)-get-patch
	@touch .$(subst .,,$@)

# Apply patches
.$(RT8192CU_T)-patch $(RT8192CU_T)-patch: .$(RT8192CU_T)-unpack
	@cp $(DIR_RT8192CU)/Makefile $(RT8192CU_SRCDIR)/
	@touch .$(subst .,,$@)

.$(RT8192CU_T)-init $(RT8192CU_T)-init: 
	@make .$(RT8192CU_T)-patch
	@touch .$(subst .,,$@)

$(RT8192CU_T)-vers: 
	@ls -1 $(KSRC)/modules/lib/modules > $(RT8192CU_SRCDIR)/version.mk

blah:
	@head -3 $(KSRC)/Makefile | cut -f3 -d" " | \
		tr "\\n" "." | sed 's/\.$$//' > $(RT8192CU_SRCDIR)/version.mk

# Build the package
$(RT8192CU_T): .$(RT8192CU_T)

.$(RT8192CU_T): .$(RT8192CU_T)-init 
	@make $(RT8192CU_T)-verify
	@$(MSG) "================================================================"
	@$(MSG2) "Building RT8192CU" $(EMSG)
	@$(MSG) "================================================================"
	cd $(RT8192CU_SRCDIR) && PATH=$(XCC_PATH) make CROSS_COMPILE=$(XCC_PREFIX)- KSRC=$(KSRC)
	@touch .$(subst .,,$@)

$(RT8192CU_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "RT8192CU Build Files ($(RT8192CU_SRCDIR))" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(RT8192CU_SRCDIR)/8192cu.ko

# Package it as an opkg 
pkg: $(RT8192CU_T) $(RT8192CU_T)-pkg

$(RT8192CU_T)-pkg: opkg-verify
	@make --no-print-directory $(RT8192CU_T)-vers
	@mkdir -p $(RT8192CU_BLDDIR)/opkg/rt8192cu/CONTROL
	@mkdir -p $(RT8192CU_BLDDIR)/opkg/rt8192cu/lib/modules/`cat $(RT8192CU_SRCDIR)/version.mk`/extra
	@cp $(RT8192CU_SRCDIR)/8192cu.ko \
		$(RT8192CU_BLDDIR)/opkg/rt8192cu/lib/modules/`cat $(RT8192CU_SRCDIR)/version.mk`/extra
	@cp $(DIR_OPKG)/control $(RT8192CU_BLDDIR)/opkg/rt8192cu/CONTROL/control
	@cp $(DIR_OPKG)/debian-binary $(RT8192CU_BLDDIR)/opkg/rt8192cu/CONTROL/debian-binary
	@cd $(RT8192CU_BLDDIR)/opkg/ && $(OPKG_DIR)/opkg-build -o root -g root -O rt8192cu
	@rm -f $(RT8192CU_SRCDIR)/version.mk
	@mkdir -p $(PKGDIR)/
	@cp $(RT8192CU_BLDDIR)/opkg/*.opk $(PKGDIR)/

pkg-clean: 
	@rm -rf $(PKGDIR) $(RT8192CU_BLDDIR)

# Clean out build tree
$(RT8192CU_T)-clean:
	@cd $(RT8192CU_SRCDIR) && make clean
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm *; fi
	@rm -f .$(RT8192CU_T) 

# Clean out everything associated with RT8192CU
$(RT8192CU_T)-clobber: 
	@rm -rf $(PKGDIR) $(RT8192CU_SRCDIR) 
	@rm -f .$(RT8192CU_T)-init .$(RT8192CU_T)-patch .$(RT8192CU_T)-unpack .$(RT8192CU_T)-get .$(RT8192CU_T)-get-patch
	@rm -f .$(RT8192CU_T) 

